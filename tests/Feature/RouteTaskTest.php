<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\Task;
use App\Models\User;

class RouteTaskTest extends TestCase
{
    use RefreshDatabase;

    /**
     * GET /
     *
     * @return void
     */
    public function testGetIndex()
    {
        $user = User::factory()->create();
        $task = Task::factory()->state(['user_id' => $user->id])->create();

        $response = $this->actingAs($user)->get('/tasks');
        $response->assertStatus(200);
        $this->assertMatchesRegularExpression('/'.$task->name.'/', $response->getContent());
    }

    /**
     * POST /task
     * test the success of task creation.
     *
     * @return void
     */
    public function testSuccessOfPostTask()
    {
        $before_tasks_count = Task::all()->count();
        $user = User::factory()->create();

        $response = $this->actingAs($user)->post('/task', ['name' => 'new task']);
        $response->assertStatus(302);

        $this->assertEquals($before_tasks_count + 1, Task::all()->count());
    }

    /**
     * POST /task
     * test the failure of task creation.
     *
     * @return void
     */
    public function testFailureOfPostTask()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->post('/task', ['name' => str_repeat('a', 256)]);
        $response->assertStatus(302);
    }

    /**
     * DELETE /task
     * test the success of task creation.
     *
     * @return void
     */
    public function testSuccessOfDeleteTask()
    {
        $user = User::factory()->create();
        $task = Task::factory()->state(['user_id' => $user->id])->create();

        $response = $this->actingAs($user)->delete("/task/$task->id");
        $response->assertStatus(302);

        $this->assertEmpty(Task::find($task->id));
    }

    /**
     * DELETE /task
     * test disable deleting other user's task
     *
     * @return void
     */
    public function testDisableOfDeletingOtherUserTask()
    {
        $user = User::factory()->create();
        $task = Task::factory()->state(['user_id' => $user->id + 1])->create();

        $response = $this->actingAs($user)->delete("/task/$task->id");
        $response->assertStatus(403);
    }
}
